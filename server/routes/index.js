const express = require('express');
const processor = require('../utils/processor')();

const indexRoutes = (router) => {
	
	router.get('/total', (req, res) => {
		//processing
		res.json({
			total: processor.total()
		});
	});

	router.get('/reg', (req, res) => {
		//processing
		res.json({
			total: processor.reg()
		});
	});	

}

module.exports = indexRoutes;