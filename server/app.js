const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const router = express.Router();
const indexRoutes = require('./routes/index');

app.use(router);

app.use('/', express.static('src'));
indexRoutes(router);

app.listen(PORT, () => {
	console.log('server running on port', PORT);
});